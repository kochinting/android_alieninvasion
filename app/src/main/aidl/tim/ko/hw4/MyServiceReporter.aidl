package tim.ko.hw4;

import tim.ko.hw4.UFOPosition;
import tim.ko.hw4.MyService;

interface MyServiceReporter{
    void report (in List<UFOPosition> ufoPosition);

}