package tim.ko.hw4;

import tim.ko.hw4.MyServiceReporter;
import tim.ko.hw4.UFOPosition;


interface MyService{
    void reset();
    void add (MyServiceReporter reporter);
    void remove (MyServiceReporter reporter);

}