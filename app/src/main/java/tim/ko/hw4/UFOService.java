package tim.ko.hw4;

/**
 * Created by TimKo on 7/16/16.
 */

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.net.HttpURLConnection;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.io.BufferedReader;
import android.os.RemoteException;

import java.util.ArrayList;
import java.util.List;

public class UFOService extends Service {

    MyService.Stub binder = new MyService.Stub() {
        public void reset() {
            n = 1;
        }
        public void add(MyServiceReporter reporter) {
            reporters.add(reporter);
        }
        public void remove(MyServiceReporter reporter) {
            reporters.remove(reporter);
        }
    };

    private List<MyServiceReporter> reporters = new ArrayList<>();

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("StartedService", "onCreate");
    }

    private volatile int n = 1;
    private UFOThread ufoThread;

    private class UFOThread extends Thread {
        List<UFOPosition> ufoPositionList = new ArrayList<>();
        UFOPosition ufoPosition = new UFOPosition();

        @Override
        public void run() {
            for (n = 0;  !isInterrupted() && n <= 50; n++) {
                Log.d("run", "count= "+n );

                for(MyServiceReporter reporter : reporters) {

                    try {

                        String urlstring = "http://javadude.com/aliens/"+n+".json";
                        URL url = new URL(urlstring);
                        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                        conn.setRequestMethod("GET");
                        conn.setRequestProperty("Accept", "application/json");

                        if (conn.getResponseCode() < 300){
                            BufferedReader br = new BufferedReader(new InputStreamReader(
                                    (conn.getInputStream())));

                            String output;
                            String content = null;

                        content= br.readLine();

                            Log.i("test", "Output from Server .... \n");

                            while ((output = br.readLine()) != null) {
                                content += output + "\n";

                            }
                            Log.d("n", Integer.toString(n));
                            Log.d("content", content);

                            try {
                                JSONArray jsonArray = new JSONArray(content);
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.optJSONObject(i);

                                    int shipNumber = Integer.parseInt(jsonObject.optString("ship".toString()));
                                    double lat = Double.parseDouble(jsonObject.optString("lat".toString()));
                                    double lon = Double.parseDouble(jsonObject.optString("lon".toString()));

                                    ufoPosition.setShipNumber(shipNumber);
                                    ufoPosition.setLat(lat);
                                    ufoPosition.setLon(lon);
                                    ufoPositionList.add(ufoPosition);

                                    try {
                                        reporter.report(ufoPositionList);
                                    } catch (RemoteException e) {
                                        Log.e(getClass().getSimpleName(), "Could not send report", e);
                                    }

                                }
                            }
                            catch (JSONException e){
                                Log.e ("exception", "unexpected JSON exception", e);
                            }


                        }else {
                            interrupt();
                        }

                        conn.disconnect();
                        Log.i("disconnect", "disconnect");

                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    interrupt();
                }
           }
            stopSelf();
        }

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        ufoThread.interrupt();
        Log.d("StartedService", "onDestroy");
    }

    @Nullable @Override public synchronized IBinder onBind(Intent intent) {
        if (ufoThread == null) {
            ufoThread = new UFOThread();

            ufoThread.start();

        }

        return binder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d("StartedService", "onUnbind");
        return super.onUnbind(intent);
    }
}