package tim.ko.hw4;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by TimKo on 7/16/16.
 */
public class UFOPosition implements Parcelable {

    private int shipNumber;
    private double lat;
    private double lon;

    public UFOPosition() {}

    public UFOPosition(int shipNumber, double lat, double lon) {
        this.shipNumber= shipNumber;
        this.lat= lat;
        this.lon= lon;
    }

    public int getShipNumber() {
        return shipNumber;
    }

    public void setShipNumber(int shipNumber) {
        this.shipNumber = shipNumber;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    // write the data for this instance
    // note that this is significantly more efficient that java serializable, because
    //   we don't need to write metadata describing the class and fields

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(shipNumber);
        dest.writeDouble(lat);
        dest.writeDouble(lon);
    }

    // read the data and create a new instance
    // note that the field MUST be named "CREATOR", all uppercase
    public static Creator<UFOPosition> CREATOR = new Creator<UFOPosition>() {
        // read the data from the parcel - note that the data read MUST be in the same
        //   order it was written in writeToParcel!
        @Override
        public UFOPosition createFromParcel(Parcel source) {
            UFOPosition ufoPosition = new UFOPosition();

            ufoPosition.setShipNumber(source.readInt());
            ufoPosition.setLat(source.readDouble());
            ufoPosition.setLon(source.readDouble());
            return ufoPosition;
        }

        @Override
        public UFOPosition[] newArray(int size) {
            return new UFOPosition[size];
        }
    };

}
