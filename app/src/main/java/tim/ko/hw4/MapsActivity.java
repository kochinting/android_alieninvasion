package tim.ko.hw4;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.content.ComponentName;
import android.content.Intent;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;
import android.os.RemoteException;
import android.Manifest;
import com.google.android.gms.maps.model.Marker;

import java.util.HashMap;
import java.util.List;
import android.support.annotation.NonNull;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import android.graphics.Color;
import java.util.Map;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private PermissionManager permissionManager = new PermissionManager();
    private SupportMapFragment mapFragment;
    private BitmapDescriptor alienShip;
    private List<UFOPosition> mapUFPPositionList;
    private String alienShipNumber="0";
    private LatLng alienPosition;
    private LatLng alienOldPosition;
    private Marker marker;
    private MyService ufoService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        alienShip = BitmapDescriptorFactory.fromResource(R.mipmap.red_ufo);

    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent= new Intent();
        intent.setClassName("tim.ko.hw4", "tim.ko.hw4.UFOService");
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);

    }

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            ufoService = MyService.Stub.asInterface(service);
            try {
                ufoService.add(reporter);
            } catch (RemoteException e) {
                Log.e(getClass().getSimpleName(), "Cannot add reporter", e);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            ufoService = null;
        }
    };

    @Override
    protected void onStop() {
        Intent intent = new Intent();
        intent.setClassName("tim.ko.hw4", "tim.ko.hw4.UFOService");
        stopService(intent);
        super.onStop();
    }

    private MyServiceReporter.Stub reporter = new MyServiceReporter.Stub() {
        @Override
        public void report(final List<UFOPosition> ufoPositionList) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Map <String, LatLng> copy = new HashMap<>();
                    LatLng alienStart = new LatLng(38.9073, -77.0365);

                    for (UFOPosition ufo : ufoPositionList){

                        Log.d("Report", Integer.toString(ufo.getShipNumber()));

                        alienShipNumber= Integer.toString(ufo.getShipNumber());
                      while (copy.get(alienShipNumber)==null){
                            alienPosition= new LatLng(ufo.getLat(), ufo.getLon());
                            copy.put(alienShipNumber, alienPosition);

                            Log.d("nullcopy", alienShipNumber);
                            Log.d("copy", alienShipNumber);
                            Log.d("copy", copy.get(alienShipNumber).toString());

                            marker= mMap.addMarker(new MarkerOptions()
                                    .position(alienPosition)
                                    .anchor(0.5f, 0.5f)
                                    .icon(alienShip)
                                    .title("AlienShip #: "+ alienShipNumber));

                            LatLngBounds bounds= new LatLngBounds(new LatLng(38.73, -77.36), new LatLng(39.02, -76.83));
                            bounds = bounds.including(alienPosition);
                            //mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, R.dimen.padding));

                        }

                        if(copy.get(alienShipNumber)!=null){
                            alienPosition= new LatLng(ufo.getLat(), ufo.getLon());
                            alienOldPosition= copy.get(alienShipNumber);

                             mMap.addPolyline(new PolylineOptions()
                                    .add(alienOldPosition, alienPosition)
                                    .width(5)
                                    .color(Color.RED));

                            copy.put(alienShipNumber, alienPosition);
                            //marker.remove();
                            mMap.addMarker(new MarkerOptions()
                                    .position(alienPosition)
                                    .anchor(0.5f, 0.5f)
                                    .icon(alienShip)
                                    .title("AlienShip #: "+ alienShipNumber));

                            Log.d("!nullcopy", alienShipNumber);
                            Log.d("copy", alienShipNumber);
                            Log.d("copy", copy.get(alienShipNumber).toString());

                            LatLngBounds bounds= new LatLngBounds(new LatLng(38.73, -77.36), new LatLng(39.02, -76.83));
                            bounds = bounds.including(alienPosition);
                            //mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, R.dimen.padding));
                            break;
                        }
                    }
                }
            });
        }
    };



    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // Add a marker in Sydney and move the camera
        LatLng alienStart = new LatLng(38.9073, -77.0365);
        mMap.addMarker(new MarkerOptions()
                .position(alienStart)
                .anchor(0.5f, 0.5f)
                .icon(alienShip)
                .title("Alien ship is here!"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(alienStart));

        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        permissionManager.run(enableMyLocationAction);



    }



    private PermissionAction enableMyLocationAction = new PermissionAction(this, null, Manifest.permission.ACCESS_FINE_LOCATION) {
        @Override
        protected void onPermissionGranted() {
            //noinspection MissingPermission
            mMap.setMyLocationEnabled(true);
        }

        @Override
        protected void onPermissionDenied() {
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionManager.handleRequestPermissionsResult(requestCode, permissions, grantResults);
    }


}
